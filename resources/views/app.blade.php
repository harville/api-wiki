<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>API Wiki</title>
<link href="{{ url('css/vendor/foundation.min.css') }}" rel="stylesheet" />
<link href="{{ url('css/app.css') }}" rel="stylesheet" />
</head>
<body>

<div class="container">
    @yield('content')
</div>

@yield('page-specific-js')

</body>
</html>
