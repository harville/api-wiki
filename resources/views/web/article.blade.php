@extends('app')

@section('content')

    <div class="article">
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <h2 class="title">{{ $article->title }}</h2>

                @if ($article->published)
                    <div class="body">{{ $article->body }}</div>
                @endif

                <div class="grid-container full related-articles">
                    <div class="grid-x grid-margin-x">

                        @foreach ($article->related as $relatedArticle)
                            <?php
                            $articleSlug = ($relatedArticle->canonical_article) ?: $relatedArticle->slug;
                            $thumbnail = ($relatedArticle->thumbnail) ?: 'default-article-thumbnail.png';
                            $titleClass = (! $relatedArticle->published) ? 'color-red' : '';
                            ?>

                            <div class="cell small-12 medium-6 large-4 related-article-card">
                                <a href="{{ url('/wiki', $articleSlug) }}">
                                    <div class="thumbnail">
                                        <img src="{{ asset('upload/images/' . $thumbnail) }}" width="112" alt="{{ $relatedArticle->title }}" />
                                    </div>

                                    <div class="related-body">
                                        <div class="title-block">
                                            <span class="title {{ $titleClass }}">
                                                {{ $relatedArticle->title }}
                                            </span>
                                            @if ($relatedArticle->canonical_article)
                                                <span class="redirect-link">Redirect link</span>
                                            @endif
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-specific-js')
    {{-- pass --}}
@endsection
