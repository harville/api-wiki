<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// articles
Route::post('page/{article}', 'Api\ArticleController@update');
Route::get('delete/{article}', 'Api\ArticleController@delete');

// articles meta
Route::get('link/{article}/to/{related}', 'Api\ArticleController@createRelation');
Route::get('redirect/{article}/to/{canonical}', 'Api\ArticleController@createRedirect');
