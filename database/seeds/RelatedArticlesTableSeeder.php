<?php

use Illuminate\Database\Seeder;

class RelatedArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('related_articles')->delete();
        DB::table('related_articles')->insert([
            [
                'article' => 'Space',
                'related' => 'Moon_landing',
            ],
            [
                'article' => 'Space',
                'related' => 'NASA',
            ],
            [
                'article' => 'Space',
                'related' => 'Jupiter',
            ],
            [
                'article' => 'Space',
                'related' => 'Dogs_in_space',
            ],
            [
                'article' => 'NASA',
                'related' => 'Space',
            ],
            [
                'article' => 'Jupiter',
                'related' => 'Space',
            ],
            [
                'article' => 'Jupiter',
                'related' => 'NASA',
            ],
        ]);
    }
}
