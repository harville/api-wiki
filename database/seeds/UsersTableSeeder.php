<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            ['name' => 'Steve Steele', 'email' => 'steven.harville.steele@gmail.com', 'password' => bcrypt('secret')],
            ['name' => 'John Smith', 'email' => 'johnsmith9382@gmail.com', 'password' => bcrypt('secret')],
        ]);
    }
}
