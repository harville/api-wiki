<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->delete();
        DB::table('articles')->insert([
            [
                'slug' => 'Space',
                'title' => 'Space',
                'body' => 'Incididunt excepteur aute magna sed irure culpa ut mollit cupidatat. In nisi dolore ut reprehenderit qui minim reprehenderit minim eiusmod. Ea deserunt occaecat nulla ut elit in proident sit sit cupidatat labore anim aliqua mollit quis ea in anim. Enim adipisicing dolor nisi laboris commodo consequat veniam ad fugiat. Ut minim reprehenderit veniam laborum elit dolore magna id adipisicing esse ex cupidatat aute aliquip. Lorem ipsum pariatur in occaecat dolore non labore occaecat veniam aliqua minim cupidatat consectetur do. Est eiusmod irure dolor velit exercitation ad ut culpa dolore sed. Labore fugiat tempor cupidatat in consectetur aute ad culpa sit nisi sint id culpa ut labore.',
                'author' => 1,
                'thumbnail' => null,
                'canonical_article' => null,
                'published' => 1,
                'created_at' => '2018-04-18 04:58:04',
                'updated_at' => '2018-04-18 05:07:31',
            ],
            [
                'slug' => 'NASA',
                'title' => 'NASA',
                'body' => '',
                'author' => 1,
                'thumbnail' => 'nasa.png',
                'canonical_article' => null,
                'published' => 0,
                'created_at' => '2018-04-18 04:58:04',
                'updated_at' => '2018-04-18 05:07:31',
            ],
            [
                'slug' => 'Jupiter',
                'title' => 'Jupiter',
                'body' => 'In labore fugiat incididunt esse cillum esse nostrud velit magna sed. Non labore in proident laboris ut aliquip culpa sunt eu velit esse dolor sit reprehenderit commodo minim adipisicing ex. Reprehenderit non aliqua ut sunt excepteur veniam veniam elit amet eu. Reprehenderit labore fugiat adipisicing dolor qui dolore magna mollit aliqua. Lorem ipsum minim ut reprehenderit quis amet dolore eiusmod ut ex. Ea ad commodo pariatur ex in fugiat magna duis dolor proident do velit. Aliqua dolor mollit consequat sed ut dolor anim irure. Deserunt ut ut ut do dolore proident amet officia ut amet non occaecat elit reprehenderit elit amet.',
                'author' => 2,
                'thumbnail' => null,
                'canonical_article' => null,
                'published' => 1,
                'created_at' => '2018-04-18 04:58:04',
                'updated_at' => '2018-04-18 05:07:31',
            ],
            [
                'slug' => 'Dogs_in_space',
                'title' => 'Dogs in space',
                'body' => 'Anim et cupidatat tempor sit et mollit dolore laborum adipisicing nostrud esse ad eu in ut elit voluptate irure. Anim pariatur deserunt minim laboris aliquip consequat in duis aute commodo cillum quis. Mollit ut nisi elit ad irure mollit commodo sed irure culpa. Eu occaecat duis sint consectetur esse in aute ex irure irure ut aliquip est. Lorem ipsum dolor nisi proident elit laboris cillum eu magna in proident officia labore. Reprehenderit ea dolor ut excepteur irure eu proident labore deserunt in esse ad cillum velit aute esse. Lorem ipsum aliquip in ea consequat sunt labore quis commodo mollit voluptate nulla cupidatat ea adipisicing cillum dolor.',
                'author' => 2,
                'thumbnail' => null,
                'canonical_article' => null,
                'published' => 1,
                'created_at' => '2018-04-18 04:58:04',
                'updated_at' => '2018-04-18 05:07:31',
            ],
            [
                'slug' => 'Moon_landing',
                'title' => 'Moon landing',
                'body' => '',
                'author' => '',
                'thumbnail' => null,
                'canonical_article' => 'NASA',
                'published' => 1,
                'created_at' => '2018-04-18 04:58:04',
                'updated_at' => '2018-04-18 05:07:31',
            ],
        ]);
    }
}
