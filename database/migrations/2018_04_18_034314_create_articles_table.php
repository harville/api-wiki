<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->string('slug');
            $table->string('title');
            $table->text('body');
            $table->integer('author')->unsigned();
            $table->binary('thumbnail')->nullable();
            $table->string('canonical_article')->nullable();
            $table->boolean('published')->default(0);
            $table->timestamps();

            $table->primary('slug');
            $table->foreign('author')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
