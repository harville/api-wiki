
# Installation

Here are the project dependencies:

- composer (I have version 1.6.3)
- PHP 7.1.3 or above
- sqlite
- PHP PDO driver for sqlite:
    - issue `php --info | grep drivers`, if sqlite listed under PDO drivers, good to go
    - ...else on mac try `sudo port install php70-sqlite`
    - ...or `brew install sqlite`, though homebrew is hesitant about overwriting sqlite3, so there's that
    - ...if on linux (not able to verify this) try `sudo apt-get install php7.0-sqlite` and `sudo service apache2 restart`

Please find included in the repo the `.env` file I used to configure the application. You will need to update some settings to get the application up and running on your production server. At minimum `DB_DATABASE` will need to to point to the absolute path of the sqlite DB file on your environment. The `database.sqlite` file is located in the `database` directory found at the root of the repo.

Run `composer install` to install the necessary PHP libraries. Execute `php artisan migrate:refresh --seed` to generate the database and populate some data (`database/database.sqlite` is available as well). Start the server with `php artisan serve --port 8142`. Request `http://localhost:8142/` or `http://127.0.0.1:8142/` to pull up the site.


# Approach

I chose Laravel to power the site. It's easy to configure. It supports many DB options. It is flexible in that there are many different ways to run the program. One of those methods is running via a virtual machine. I usually go this route because it's self-contained and portable. It's too bad my old desktop was too slow to run it. Also, the port forwarding was giving me problems. Instead I decided to serve the site via artisan as it allows port specification. I chose sqlite DB also because of it's portability. It is fine for now. As the project scales it becomes less fine. Foundation powers the responsive grid. Usually I mix that with SASS and Gulp, but the short deadline prevented that from happening.


# Discussion

The best way to learn a project and the problems you will face is to dive in. The biggest issue is how best to deal with the unknown. The big unknown here are non-existent links. The comps show these with text in red. Where do they come from? I assume they carry over from related links and redirects: actions taken on other pages. So that is how I coded the application to work. If an article is set to redirect or link to a non-existent article, I create the article using the slug provided. It's told that the application isn't aware of these, but the comps imply the opposite. I'm not sure how a non-existent article has a custom thumbnail otherwise. This may be all wrong, but it's okay. We've learned some things along the way.

It took a little time to realize that I shouldn't be using IDs as currency to relate data as I move about the application. I started with an ID approach, the introduced GUID slugs, then confused everything by mixing and matching them before I stepped back and realized slugs are king in this application. So now they are the source of truth moving forward.

Articles contain a `canonical_article` DB field that controls whether `body` or `author` fields are ever rendered. If the field is set to another article's slug, the link will reflect the path to the canonical article. As such, clicking the link takes the user directly to the canonical article, thus the `body` and `author` fields of the "placeholder" related article will never be rendered. Users can still edit the placeholder to their liking and if the `canonical_article` field is ever unset in the future, it will act as a regular article. There is no 301 redirect to the canonical article. I wasn't initially sure if that was needed or part of the scope of this exploratory work. It has become clear as I've worked on the project that the redirects may be necessary to address the problem of stale pages with improper links. This will only get worse as the project scales. Redirects would address the problem by invoking just-in-time decisioning at the stateless (stalefree) server level. There may be other novel approaches, but 301s are a time-tested approach.

I've implemented soft deletes. This is usually preferable to actually deleting records from the database. The `published` field acts as a toggle for articles when enabling on creation and disabling on deletion. When `published` is false, the application halfway pretends to be unaware of non-existent links, but it knows.

I'm not sure if a related link that redirects to an unpublished "non-existent" article should show in red font. Currently it does not. It could work but I'd have to make DB querying a little greedier. This would take a little time, may hinder processing speed, and would add complexity to the site.


# Next steps

- create a front-end form layer as a user interface to the API so that users can create/edit the new summary articles and related links more easily and intuitively
    - fields include: slug, title, body, thumbnail, canonical_article, published
    - author field should be populated with the logged-in user
    - image upload functionality has not been implemented, need to do that and save the path to the new asset in `thumbnail` field
- in the interest of time, some functionality currently exists in views: these should be extracted to the controller and moved to a service layer
- controller methods are too fat and have code smell from too many nested blocks - these need to be refactored
- clean up the styles - i was in a hurry and the CSS already feels gross - should update to SASS, modularize, and gulpify assets

