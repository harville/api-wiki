<?php

namespace App\Http\Controllers\Api;

use App\Article;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    public function update(string $slug)
    {
        $message = 'There was a problem creating ' . $slug;

        $rawRequestBody = $this->request->getContent();
        $escapedRequestBody = e($rawRequestBody);

        try {
            $article = Article::find($slug);
            if ($article) {
                $article->setBody($escapedRequestBody);
                $article->save();
                $message = $article->title . ' has been successfully updated';
            } else {
                $fields = [
                    'slug'              => $slug,
                    'title'             => $slug,
                    'body'              => $escapedRequestBody,
                    'author'            => 1,
                    'thumbnail'         => null,
                    'canonical_article' => null,
                    'published'         => 1,
                ];

                $article = new Article;
                if ($article->generate($fields)) {
                    $message = $article->title . ' has been successfully created';
                } else {
                    $message = 'Error: ' . $article->getError();
                }
            }
        } catch (\Exception $e) {
            $message = 'Error: ' . $e->getMessage();
        }

        return view('api.message', ['message' => $message]);
    }

    public function delete(string $slug = '')
    {
        $message = 'There was a problem deleting ' . $slug;

        try {
            $article = Article::find($slug);
            $message = $article->title . ' is already deleted';
            if ($article->published) {
                $article->setPublished(false);
                $article->save();
                $message = $article->title . ' has been successfully deleted';
            }
        } catch (\Exception $e) {
            $message = 'Error: ' . $e->getMessage();
        }

        return view('api.message', ['message' => $message]);
    }

    public function createRelation(string $articleSlug, string $relatedSlug)
    {
        $message = 'There was a problem linking ' . $articleSlug . ' to ' . $relatedSlug;

        try {
            $article = Article::find($articleSlug);
            if ($article) {
                $message = $relatedSlug . ' link already exists on '. $article->title;
                $isPreexisting = $article->relateArticle($relatedSlug);
                if (! $isPreexisting) {
                    $article->save();
                    $message = $relatedSlug . ' link has been successfully created on '. $article->title;
                }

                // create related article if nonexistent
                if (! Article::find($relatedSlug)) {
                    $fields = [
                        'slug'              => $relatedSlug,
                        'title'             => $relatedSlug,
                        'body'              => '',
                        'author'            => 1,
                        'thumbnail'         => null,
                        'canonical_article' => null,
                        'published'         => 0,
                    ];

                    $article = new Article;
                    if (! $article->generate($fields)) {
                        $message = 'Error: ' . $article->getError();
                    }
                }
            }
        } catch (\Exception $e) {
            $message = 'Error: ' . $e->getMessage();
        }

        return view('api.message', ['message' => $message]);
    }

    public function createRedirect(string $articleSlug, string $canonicalSlug)
    {
        $message = 'There was a problem creating redirect from ' . $articleSlug . ' to ' . $canonicalSlug;

        try {
            $article = Article::find($articleSlug);
            if ($article) {
                $message = $article->title . ' is already redirecting to '. $canonicalSlug;
                if ($article->canonical_article != $canonicalSlug) {
                    $article->setCanonicalArticle($canonicalSlug);
                    $article->save();
                    $message = $article->title . ' has been successfully redirected to ' . $canonicalSlug;
                }

                // create canonical article if nonexistent
                if (! Article::find($canonicalSlug)) {
                    $fields = [
                        'slug'              => $canonicalSlug,
                        'title'             => $canonicalSlug,
                        'body'              => '',
                        'author'            => 1,
                        'thumbnail'         => null,
                        'canonical_article' => null,
                        'published'         => 0,
                    ];

                    $article = new Article;
                    if (! $article->generate($fields)) {
                        $message = 'Error: ' . $article->getError();
                    }
                }
            }
        } catch (\Exception $e) {
            $message = 'Error: ' . $e->getMessage();
        }

        return view('api.message', ['message' => $message]);
    }
}
