<?php

namespace App\Http\Controllers\Web;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function get(string $slug)
    {
        return view('web.article', ['article' => Article::findOrFail($slug)]);
    }
}
