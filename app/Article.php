<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $primaryKey = 'slug';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $error = null;

    protected $defaultFieldValues = [
        'slug'              => null,
        'title'             => null,
        'body'              => null,
        'author'            => 1,
        'thumbnail'         => null,
        'canonical_article' => null,
        'published'         => 0,
    ];

    protected $fillable = [
        'slug',
        'title',
        'body',
        'author',
        'thumbnail',
        'canonical_article',
        'published'
    ];

    public function setSlug(string $slug = '')
    {
        $this->slug = $slug;
    }

    public function setTitle(string $title = '')
    {
        $this->title = $title;
    }

    public function setBody(string $body = '')
    {
        $this->body = $body;
    }

    public function setAuthor(int $author = 1)
    {
        $this->author = $author;
    }

    public function setThumbnail(string $thumbnail = null)
    {
        $this->thumbnail = $thumbnail;
    }

    public function setCanonicalArticle(string $canonicalArticle = null)
    {
        $this->canonical_article = $canonicalArticle;
    }

    public function setPublished(bool $published = false)
    {
        $this->published = $published;
    }

    public function setError(string $error = null)
    {
        $this->error = $error;
    }

    public function getError()
    {
        return $this->error;
    }

    public function generate(array $fields = [])
    {
        $isGenerated = true;

        $fields = array_merge($this->defaultFieldValues, $fields);
        extract($fields);

        $this->setSlug($slug);
        $this->setTitle($title);
        $this->setBody($body);
        $this->setAuthor($author);
        $this->setThumbnail($thumbnail);
        $this->setCanonicalArticle($canonical_article);
        $this->setPublished($published);

        try {
            $this->save();
        } catch (\Exception $e) {
            $this->setError($e->getMessage());
            $isGenerated = false;
        }

        return $isGenerated;
    }

    public function related()
    {
        return $this->belongsToMany('App\Article', 'related_articles', 'article', 'related');
    }

    public function relateArticle(string $relatedSlug)
    {
        $isPreexisting = $this->related->contains($relatedSlug);
        if (! $isPreexisting) {
            $this->related()->attach($relatedSlug);
        }
        return $isPreexisting;
    }

    public function unrelateArticle(string $relatedSlug)
    {
        $isPreexisting = $this->related->contains($relatedSlug);
        if ($isPreexisting) {
            $this->related()->detach($relatedSlug);
        }
        return $isPreexisting;
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }
}
